Download and process Vasaloppets results data using R
=============================================

Take a look at [data.markuskainu.fi/vasaloppetdata](https://data.markuskainu.fi/vasaloppetdata)!

This project allows you to download and process [Vasaloppets](https://en.wikipedia.org/wiki/Vasaloppet) results data from [results.vasaloppet.se](http://results.vasaloppet.se/).

It outputs a results with split times for both women and men in the Sundays main event in `.parquet` and `.csv` -formats. This setup has been tested to process data from 2016-2024.


## License

MIT-license

## devtools::session_info

```r
> devtools::session_info()
─ Session info ────────────────────────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.3.3 (2024-02-29)
 os       Ubuntu 22.04.4 LTS
 system   x86_64, linux-gnu
 ui       RStudio
 language (EN)
 collate  fi_FI.UTF-8
 ctype    fi_FI.UTF-8
 tz       Europe/Helsinki
 date     2024-03-07
 rstudio  2023.12.0+369 Ocean Storm (server)
 pandoc   NA

─ Packages ────────────────────────────────────────────────────────────────────────────────────────────────────────────
 package     * version  date (UTC) lib source
 archive       1.1.7    2023-12-11 [3] RSPM (R 4.3.0)
 arrow       * 14.0.2.1 2024-02-23 [3] RSPM (R 4.3.0)
 assertthat    0.2.1    2019-03-21 [3] RSPM (R 4.2.0)
 bit           4.0.5    2022-11-15 [3] RSPM (R 4.2.0)
 bit64         4.0.5    2020-08-30 [3] RSPM (R 4.2.0)
 blob          1.2.4    2023-03-17 [3] RSPM (R 4.2.0)
 cachem        1.0.8    2023-05-01 [3] RSPM (R 4.2.0)
 cli           3.6.2    2023-12-11 [3] RSPM (R 4.3.0)
 colorspace    2.1-0    2023-01-23 [3] RSPM (R 4.2.0)
 crayon        1.5.2    2022-09-29 [3] RSPM (R 4.2.0)
 DBI           1.2.2    2024-02-16 [3] RSPM (R 4.3.0)
 devtools      2.4.5    2022-10-11 [3] RSPM (R 4.2.0)
 digest        0.6.34   2024-01-11 [3] RSPM (R 4.3.0)
 dplyr       * 1.1.4    2023-11-17 [3] RSPM (R 4.3.0)
 ellipsis      0.3.2    2021-04-29 [3] RSPM (R 4.2.0)
 evaluate      0.23     2023-11-01 [3] RSPM (R 4.3.0)
 fansi         1.0.6    2023-12-08 [3] RSPM (R 4.3.0)
 fastmap       1.1.1    2023-02-24 [3] RSPM (R 4.2.0)
 fs            1.6.3    2023-07-20 [3] RSPM (R 4.2.0)
 generics      0.1.3    2022-07-05 [3] RSPM (R 4.2.0)
 ggplot2       3.5.0    2024-02-23 [3] RSPM (R 4.3.0)
 glue        * 1.7.0    2024-01-09 [3] RSPM (R 4.3.0)
 gtable        0.3.4    2023-08-21 [3] RSPM (R 4.2.0)
 here          1.0.1    2020-12-13 [3] RSPM (R 4.2.0)
 hms           1.1.3    2023-03-21 [3] RSPM (R 4.2.0)
 htmltools     0.5.7    2023-11-03 [3] RSPM (R 4.3.0)
 htmlwidgets   1.6.4    2023-12-06 [3] RSPM (R 4.3.0)
 httpuv        1.6.14   2024-01-26 [3] RSPM (R 4.3.0)
 httr          1.4.7    2023-08-15 [3] RSPM (R 4.2.0)
 knitr         1.45     2023-10-30 [3] RSPM (R 4.3.0)
 later         1.3.2    2023-12-06 [3] RSPM (R 4.3.0)
 lifecycle     1.0.4    2023-11-07 [3] RSPM (R 4.3.0)
 lubridate     1.9.3    2023-09-27 [3] RSPM (R 4.3.0)
 magrittr      2.0.3    2022-03-30 [3] RSPM (R 4.2.0)
 memoise       2.0.1    2021-11-26 [3] RSPM (R 4.2.0)
 mime          0.12     2021-09-28 [3] RSPM (R 4.2.0)
 miniUI        0.1.1.1  2018-05-18 [3] RSPM (R 4.2.0)
 munsell       0.5.0    2018-06-12 [3] RSPM (R 4.2.0)
 pillar        1.9.0    2023-03-22 [3] RSPM (R 4.2.0)
 pkgbuild      1.4.3    2023-12-10 [3] RSPM (R 4.3.0)
 pkgconfig     2.0.3    2019-09-22 [3] RSPM (R 4.2.0)
 pkgload       1.3.4    2024-01-16 [3] RSPM (R 4.3.2)
 profvis       0.3.8    2023-05-02 [3] RSPM (R 4.2.0)
 promises      1.2.1    2023-08-10 [3] RSPM (R 4.2.0)
 purrr         1.0.2    2023-08-10 [3] RSPM (R 4.2.0)
 R6            2.5.1    2021-08-19 [3] RSPM (R 4.2.0)
 Rcpp          1.0.12   2024-01-09 [3] RSPM (R 4.3.0)
 readr         2.1.5    2024-01-10 [3] RSPM (R 4.3.0)
 remotes       2.4.2.1  2023-07-18 [3] RSPM (R 4.2.0)
 rlang         1.1.3    2024-01-10 [3] RSPM (R 4.3.0)
 RPostgres     1.4.6    2023-10-22 [3] RSPM (R 4.3.0)
 rprojroot     2.0.4    2023-11-05 [3] RSPM (R 4.3.0)
 rstudioapi    0.15.0   2023-07-07 [3] RSPM (R 4.2.0)
 rvest       * 1.0.4    2024-02-12 [3] RSPM (R 4.3.0)
 scales        1.3.0    2023-11-28 [3] RSPM (R 4.3.0)
 sessioninfo   1.2.2    2021-12-06 [3] RSPM (R 4.2.0)
 shiny         1.8.0    2023-11-17 [3] RSPM (R 4.3.0)
 stringi       1.8.3    2023-12-11 [3] RSPM (R 4.3.0)
 stringr     * 1.5.1    2023-11-14 [3] RSPM (R 4.3.0)
 tibble        3.2.1    2023-03-20 [3] RSPM (R 4.3.0)
 tidyr       * 1.3.1    2024-01-24 [3] RSPM (R 4.3.0)
 tidyselect    1.2.0    2022-10-10 [3] RSPM (R 4.2.0)
 timechange    0.3.0    2024-01-18 [3] RSPM (R 4.3.0)
 tzdb          0.4.0    2023-05-12 [3] RSPM (R 4.2.0)
 urlchecker    1.0.1    2021-11-30 [3] RSPM (R 4.2.0)
 usethis       2.2.3    2024-02-19 [3] RSPM (R 4.3.0)
 utf8          1.2.4    2023-10-22 [3] RSPM (R 4.3.0)
 vctrs         0.6.5    2023-12-01 [3] RSPM (R 4.3.0)
 vroom         1.6.5    2023-12-05 [3] RSPM (R 4.3.0)
 withr         3.0.0    2024-01-16 [3] RSPM (R 4.3.2)
 xfun          0.42     2024-02-08 [3] RSPM (R 4.3.0)
 xml2          1.3.6    2023-12-04 [3] RSPM (R 4.3.0)
 xtable        1.8-4    2019-04-21 [3] RSPM (R 4.2.0)

 [1] /home/aurelius/R/x86_64-pc-linux-gnu-library/4.3
 [2] /usr/local/lib/R/site-library
 [3] /usr/lib/R/site-library
 [4] /usr/lib/R/library
```